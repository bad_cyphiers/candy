package com.cmc.candy.schedule;

import com.cmc.candy.candidate.entity.po.CandidateEntity;
import com.cmc.candy.candidate.enums.StatusEnum;
import com.cmc.candy.candidate.repository.CandidateRepositoryCustom;
import com.cmc.candy.common.utils.CustomMailSender;
import com.cmc.candy.user.entity.po.UserCandyEntity;
import com.cmc.candy.user.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@Primary
@Component
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class Schedule {

    @Value("${spring.feature.cron.job.check-candidate-pending-two-months.enable}")
    boolean checkCandidatePendingTwoMonthsCronIsEnable;
    @Value("${spring.mail.username}")
    String mailSenderName;
    final CustomMailSender customMailSender;
    final CandidateRepositoryCustom candidateRepositoryCus;
    final UserRepository userRepository;


    @Async
    @Transactional
    //cron with value "0 9 * * * MON-FRI" means this cronjob start process at 9AM every weekday
    @Scheduled(cron = "${spring.feature.cron.job.check-candidate-pending-two-months.cron}")
    public void checkCandidatePendingTwoMonths() {
        if (!checkCandidatePendingTwoMonthsCronIsEnable) return;
        // check for candidates with status "available" over 2 months
        List<CandidateEntity> listOfCandidadites = candidateRepositoryCus.findAllCandidateWithStatusAndOverMonths(StatusEnum.AVAILABLE.name(), 2);
        if (listOfCandidadites.isEmpty()) return;

        // send email to remind HR (users) according to those candidates via users' email address
        StringBuilder candidates = new StringBuilder();
        candidates.append("<b><table>\n");
        candidates.append("<thead><tr><th>CANDIDATE ID</th> \n <th>FULL NAME</th> \n <th>CREATE DATE</th> </tr></thead>");
        candidates.append("<tbody>");
        listOfCandidadites.forEach(e -> candidates.append("<tr>").append("<td>").append(e.getId()).append("</td>").append("<td>").append(e.getFirstName())
                .append(" ").append(e.getName()).append("</td>").append("<td>").append(e.getCreatedDate()).append("</td>").append("</tr>\n"));
        candidates.append("</tbody>");
        candidates.append("</table></b>");

        List<UserCandyEntity> listOfUsers = userRepository.findAll();
        if (listOfUsers.isEmpty()) return;

        List<String> listOfUsersEmail = new ArrayList<>();
        listOfUsers.forEach(e -> listOfUsersEmail.add(e.getEmail()));

        //send email to user address
        String content = "<!-- UUID --!>"
                + "<p>Those candidates have pending for over 2 months</p>"
                + "<p>Please check them again and keep the process further!</p>"
                + "<p>The list is following:</p>"
                + candidates.toString();

        CustomMailSender.MailRequest mailRequest = new CustomMailSender.MailRequest();
        mailRequest.setTo(listOfUsersEmail);
        mailRequest.setFrom(mailSenderName);
        mailRequest.setSubject("Remind HR Group about candidate with status pending for over 2 months!");
        mailRequest.setContent(content);

        customMailSender.send(mailRequest, Throwable::printStackTrace);
        log.info("cronjob for checking candidates with status available for over 2 months and sending emails to remind HR group.");
        //-------------------
    }
}
