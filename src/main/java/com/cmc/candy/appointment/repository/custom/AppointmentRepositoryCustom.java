package com.cmc.candy.appointment.repository.custom;

import com.cmc.candy.appointment.entity.dto.response.AppointmentDTO;
import com.cmc.candy.appointment.entity.po.AppointmentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AppointmentRepositoryCustom extends JpaRepository<AppointmentEntity, Long> {


    @Query(value = "SELECT \n" +
            "    appoint.id AS appointmentId,\n" +
            "    concat(candy.first_name , candy.last_name) AS fullName,\n" +
            "    appoint.create_date AS dateOfApplication,\n" +
            "    appoint.interview_date AS dateOfInterview,\n" +
            "    candy.position AS positionName,\n" +
            "    candy.phone_number AS phoneNumber,\n" +
            "    candy.skills AS skill\n" +
            "FROM\n" +
            "    appointments AS appoint\n" +
            "        LEFT JOIN\n" +
            "    candidates AS candy ON appoint.candidate_id = candy.id\n" +
            "WHERE\n" +
            "    (candy.position LIKE :information \n" +
            "        OR appoint.interviewers LIKE :information) \n" +
            "        AND ((:upcoming = 0)\n" +
            "        OR (DATEDIFF(appoint.interview_date, NOW()) BETWEEN 0 AND :numberOfDay))\n" +
            "ORDER BY appoint.create_date DESC",
            countQuery = "SELECT \n" +
                    "    appoint.id AS appointmentId,\n" +
                    "    concat(candy.first_name , candy.last_name) AS fullName,\n" +
                    "    appoint.create_date AS dateOfApplication,\n" +
                    "    appoint.interview_date AS dateOfInterview,\n" +
                    "    candy.position AS positionName,\n" +
                    "    candy.phone_number AS phoneNumber,\n" +
                    "    candy.skills AS skill\n" +
                    "FROM\n" +
                    "    appointments AS appoint\n" +
                    "        LEFT JOIN\n" +
                    "    candidates AS candy ON appoint.candidate_id = candy.id\n" +
                    "WHERE\n" +
                    "    (candy.position LIKE :information \n" +
                    "        OR appoint.interviewers LIKE :information) \n" +
                    "        AND ((:upcoming = 0)\n" +
                    "        OR (DATEDIFF(appoint.interview_date, NOW()) BETWEEN 0 AND :numberOfDay))\n" +
                    "ORDER BY appoint.create_date DESC",
            nativeQuery = true)
    Page<AppointmentDTO> searchAppointment(@Param("upcoming") Boolean upComing, @Param("numberOfDay") int day, @Param("information") String information, Pageable pageable);
}
