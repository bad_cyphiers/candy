package com.cmc.candy.appointment.repository;

import com.cmc.candy.appointment.entity.po.AppointmentEntity;
import com.cmc.candy.appointment.repository.custom.AppointmentRepositoryCustom;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface AppointmentRepository extends AppointmentRepositoryCustom {
    List<AppointmentEntity> findAllByInterviewDateBetween(Instant from, Instant to);
}
