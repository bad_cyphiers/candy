package com.cmc.candy.appointment.repository;

import com.cmc.candy.appointment.entity.po.ApplicationTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationTypeRepository extends JpaRepository<ApplicationTypeEntity, Long> {
}
