package com.cmc.candy.appointment.schedule;


import com.cmc.candy.appointment.entity.po.ApplicationTypeEntity;
import com.cmc.candy.appointment.entity.po.AppointmentEntity;
import com.cmc.candy.appointment.repository.ApplicationTypeRepository;
import com.cmc.candy.appointment.repository.AppointmentRepository;
import com.cmc.candy.candidate.entity.po.CandidateEntity;
import com.cmc.candy.candidate.repository.CandidateRepository;
import com.cmc.candy.common.utils.CustomMailSender;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Collections;
import java.util.List;

@Configuration
@RequiredArgsConstructor
public class ScheduleConfig {

    private final CustomMailSender customMailSender;
    private final ApplicationTypeRepository applicationTypeRepository;
    private final AppointmentRepository appointmentRepository;
    private final CandidateRepository candidateRepository;

    //    @Scheduled(cron = "*/2 * * * * *")
    @Scheduled(fixedDelay = 30000)
    @Transactional
    public void triggerRemindEmails() {
        Instant to = Instant.now().plus(3, ChronoUnit.HOURS);
        List<AppointmentEntity> appointmentEntities = appointmentRepository.findAllByInterviewDateBetween(Instant.now(), to);

        appointmentEntities.forEach(appointmentEntity -> {

            ApplicationTypeEntity applicationType = applicationTypeRepository.getById(appointmentEntity.getApplicationTypeId());
            CandidateEntity candidateEntity = candidateRepository.getById(appointmentEntity.getCandidateID());

            //send email to user address
            String content = "<!-- UUID --!>"
                    + "<p>[CMC Global] Remind interview invitation _ " + applicationType.getType() + "&nbsp;" + candidateEntity.getPosition()
                    + "</p>" + "<p>Dear," + "&nbsp;" + candidateEntity.getFirstName() + "&nbsp;" + candidateEntity.getName() + "</p>" +
                    "<p>You have an upcoming interview. As discussed, we would like to "
                    + "invite you to attend the interview with our company. Please have a look "
                    + "and then attend the interview on time: </p>"
                    + "<ul>"
                    + "<li> Title:  " + applicationType.getType() + candidateEntity.getPosition() + "</li>"
                    + "<li> Date and time:  " + appointmentEntity.getInterviewDate() + "</li>"
                    + "<li> Interviewer:  " + appointmentEntity.getInterviewers() + "</li>"
                    + "<li> Contact point: " + appointmentEntity.getAgentSupport() + "</li>"
                    + "</ul>"
                    + "<p>Notes: " + "<br>"
                    + "<p> - Please confirm this invitation whether you would attend the interview. </p> "
                    + "<p> - Should you have any further question, please do not hesitate to contact me via email anytime.</p>"
                    + "<br>"
                    + "<p>Yours Sincerely, </p>" + "<p>" + appointmentEntity.getAgentSupport() + "</p>";


            CustomMailSender.MailRequest mailRequest = new CustomMailSender.MailRequest();
            mailRequest.setTo(Collections.singletonList(appointmentEntity.getCandidateEmail()));
            mailRequest.setFrom("haivuhai184@gmail.com");
            mailRequest.setSubject("Remind email for upcoming interview from CMC");
            mailRequest.setContent(content);

            if (appointmentEntity.getIsReminded() == 0) {
                appointmentEntity.setIsReminded(1);
                customMailSender.send(mailRequest, Throwable::printStackTrace);

            }
            //-------------------
        });
    }
}
