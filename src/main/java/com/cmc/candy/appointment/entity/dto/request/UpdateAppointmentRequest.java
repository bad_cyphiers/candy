package com.cmc.candy.appointment.entity.dto.request;

import lombok.Builder;
import lombok.Getter;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Builder(toBuilder = true)
public class UpdateAppointmentRequest {
    private String senderEmail;
    private String candidateEmail;
    private String title;
    private List<String> cc;
    private List<String> bc;
    private String content;
    private String interviewers;
    private String agentSupport;
    private long candidateID;
    private String topic;
    private String description;
    private Long node;
    private String feedback;
    private int finalDecision;
    private OffsetDateTime obDate;
    private OffsetDateTime interviewDate;
    private String rejectReason;
    private int interviewDuration; // It's in minutes
    private long applicantType;
}
