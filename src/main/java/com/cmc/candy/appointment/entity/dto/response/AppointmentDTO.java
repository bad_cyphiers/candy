package com.cmc.candy.appointment.entity.dto.response;

import java.time.Instant;

public interface AppointmentDTO {
    Long getAppointmentId();

    String getFullName();

    Instant getDateOfApplication();

    Instant getDateOfInterview();

    String getPositionName();

    String getPhoneNumber();

    String getSkill();

    Instant getCreatedDate();


}
