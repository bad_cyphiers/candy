package com.cmc.candy.appointment.entity.po;

import com.cmc.candy.candidate.entity.po.CandidateEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;


@Entity
@Table(name = "appointments", uniqueConstraints = {@UniqueConstraint(columnNames = "candidate_id")})
@Getter
@Setter

public class AppointmentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sender_email")
    private String senderEmail;

    @Column(name = "candidate_email")
    private String candidateEmail;

    @Column(name = "title")
    private String title;

    @Column(name = "cc")
    private String cc;

    @Column(name = "bc")
    private String bc;

    @Column(name = "content")
    private String content;

    @Column(name = "interviewers")
    private String interviewers;

    @Column(name = "agent_support")
    private String agentSupport;

    @Column(name = "candidate_id")
    private Long candidateID;

    @Column(name = "topic")
    private String topic;

    @Column(name = "description")
    private String description;

    @Column(name = "node")
    private Long node;

    @Column(name = "feedback")
    private String feedback;

    @Column(name = "final_decision")
    private int finalDecision;

    @Column(name = "ob_date")
    private Instant obDate;

    @Column(name = "interview_date")
    private Instant interviewDate;

    @Column(name = "reject_reason")
    private String rejectReason;

    @Column(name = "interview_duration")
    private int interviewDuration;

    @Column(name = "application_type")
    private long applicationTypeId;

    @Column(name = "is_deleted")
    private int isDeleted;

    @Column(name = "create_date")
    private Instant createDate;

    @Column(name = "update_date")
    private Instant updatedDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "candidate_id", referencedColumnName = "id", insertable = false, updatable = false)
    private CandidateEntity candidates;

    @ManyToOne
    @JoinColumn(name = "application_type", referencedColumnName = "id", insertable = false, updatable = false)
    private ApplicationTypeEntity applicationType;

    @Column(name = "is_reminded")
    private int isReminded;

}
