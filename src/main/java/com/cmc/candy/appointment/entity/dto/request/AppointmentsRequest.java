package com.cmc.candy.appointment.entity.dto.request;

import lombok.Builder;
import lombok.Getter;

import java.time.OffsetDateTime;
import java.util.List;


@Getter
@Builder(toBuilder = true)
public class AppointmentsRequest {
    private String senderEmail;
    private String candidateEmail;
    private String title;
    private List<String> cc;
    private List<String> bc;
    private String content;
    private String interviewers;
    private String agentSupport;
    private Long candidateID;
    private String topic;
    private String description;
    private Long node;
    private String feedback;
    private int finalDecision;
    private OffsetDateTime obDate;
    private OffsetDateTime interviewDate;
    private String rejectReason;
    private int interviewDuration;
    private long applicantType;
}
