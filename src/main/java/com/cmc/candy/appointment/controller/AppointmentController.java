package com.cmc.candy.appointment.controller;


import com.cmc.candy.appointment.entity.dto.request.AppointmentsRequest;
import com.cmc.candy.appointment.entity.dto.request.UpdateAppointmentRequest;
import com.cmc.candy.appointment.entity.dto.response.AppointmentDTO;
import com.cmc.candy.appointment.entity.po.AppointmentEntity;
import com.cmc.candy.appointment.service.AppointmentsService;
import com.cmc.candy.candidate.entity.dto.response.PageDTO;
import com.cmc.candy.common.entity.AppointmentFilterHandler;
import com.cmc.candy.common.entity.CandyFilter;
import com.cmc.candy.common.utils.CommonValidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/v1/appointments")
@CrossOrigin(origins = "*")
public class AppointmentController {
    @Resource
    AppointmentsService appointmentsService;

    @PostMapping
    public ResponseEntity<AppointmentEntity> addAppointments(@RequestBody AppointmentsRequest appointmentsRequest) {
        CommonValidate.dateAfter(appointmentsRequest.getInterviewDate());
        CommonValidate.dateBefore(appointmentsRequest.getObDate());
        return ResponseEntity.ok(appointmentsService.addAppointment(appointmentsRequest));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<AppointmentEntity> deleteAppointmentEntity(@PathVariable Long id) {
        return ResponseEntity.ok(appointmentsService.deleteAppointmentEntity(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<AppointmentEntity> updateAppointment(@PathVariable Long id,
                                                               @RequestBody UpdateAppointmentRequest updateAppointmentRequest) {
        CommonValidate.dateAfter(updateAppointmentRequest.getInterviewDate());
        CommonValidate.dateBefore(updateAppointmentRequest.getObDate());
        return ResponseEntity.ok(appointmentsService.updateAppointmentEntity(id, updateAppointmentRequest));
    }

    @PostMapping("/search")
    public ResponseEntity<PageDTO<AppointmentDTO>> searchAppointment(
            @RequestBody List<CandyFilter> candyFilters,
            @RequestParam("information") String information,
            Pageable pageable) {
        return ResponseEntity.ok(appointmentsService.filterSearchAppointment(new AppointmentFilterHandler(candyFilters), information, pageable));
    }

    @PutMapping("/giveFeedback/{id}")
    public ResponseEntity<AppointmentEntity> giveFeedBack(@PathVariable Long candidateId, @RequestBody UpdateAppointmentRequest updateAppointmentRequest) {
        CommonValidate.inputNotNull(updateAppointmentRequest.getFeedback());
        return ResponseEntity.ok(appointmentsService.giveFeedBack(candidateId, updateAppointmentRequest));
    }

    @PutMapping("/finalDecision/{id}")
    public ResponseEntity<AppointmentEntity> finalDecision(@PathVariable Long id, @RequestBody UpdateAppointmentRequest updateAppointmentRequest) {
        return ResponseEntity.ok(appointmentsService.finalDecision(id, updateAppointmentRequest));
    }

    @PutMapping("/suspendInterview/{id}")
    public ResponseEntity<AppointmentEntity> suspendInterview(@PathVariable Long id, @RequestBody UpdateAppointmentRequest updateAppointmentRequest) {
        return ResponseEntity.ok(appointmentsService.suspendInterview(id, updateAppointmentRequest));
    }


}
