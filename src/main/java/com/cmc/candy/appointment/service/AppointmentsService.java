package com.cmc.candy.appointment.service;

import com.cmc.candy.appointment.entity.dto.request.AppointmentsRequest;
import com.cmc.candy.appointment.entity.dto.request.UpdateAppointmentRequest;
import com.cmc.candy.appointment.entity.dto.response.AppointmentDTO;
import com.cmc.candy.appointment.entity.po.AppointmentEntity;
import com.cmc.candy.candidate.entity.dto.response.PageDTO;
import com.cmc.candy.common.entity.FilterCriteria;
import org.springframework.data.domain.Pageable;


public interface AppointmentsService {
    AppointmentEntity addAppointment(AppointmentsRequest appointmentsRequest);

    AppointmentEntity deleteAppointmentEntity(Long id);

    AppointmentEntity updateAppointmentEntity(Long id, UpdateAppointmentRequest updateAppointmentRequest);

    PageDTO<AppointmentDTO> filterSearchAppointment(FilterCriteria criteria, String information, Pageable pageable);

    AppointmentEntity giveFeedBack(Long id, UpdateAppointmentRequest updateAppointmentRequest);

    AppointmentEntity finalDecision(Long id, UpdateAppointmentRequest updateAppointmentRequest);

    AppointmentEntity suspendInterview(Long id, UpdateAppointmentRequest updateAppointmentRequest);

}
