package com.cmc.candy.appointment.service.impl;

import com.cmc.candy.appointment.entity.dto.request.AppointmentsRequest;
import com.cmc.candy.appointment.entity.dto.request.UpdateAppointmentRequest;
import com.cmc.candy.appointment.entity.dto.response.AppointmentDTO;
import com.cmc.candy.appointment.entity.po.ApplicationTypeEntity;
import com.cmc.candy.appointment.entity.po.AppointmentEntity;
import com.cmc.candy.appointment.repository.ApplicationTypeRepository;
import com.cmc.candy.appointment.repository.AppointmentRepository;
import com.cmc.candy.appointment.service.AppointmentsService;
import com.cmc.candy.candidate.entity.dto.response.PageDTO;
import com.cmc.candy.candidate.entity.po.CandidateEntity;
import com.cmc.candy.candidate.repository.CandidateRepository;
import com.cmc.candy.common.entity.CandyFilter;
import com.cmc.candy.common.entity.FilterCriteria;
import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.enums.StatusEnum;
import com.cmc.candy.common.throwable.BusinessException;
import com.cmc.candy.common.utils.CommonUtils;
import com.cmc.candy.common.utils.CustomMailSender;
import com.cmc.candy.common.utils.DateUtils;
import com.cmc.candy.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Collections;
import java.util.Optional;


@Service
@Slf4j
@RequiredArgsConstructor
public class AppointmentsServiceImpl implements AppointmentsService {

    private final CustomMailSender customMailSender;
    private final ApplicationTypeRepository applicationTypeRepository;
    private final AppointmentRepository appointmentRepository;

    private final CandidateRepository candidateRepository;


    @SneakyThrows
    @Override
    @Transactional
    public AppointmentEntity addAppointment(AppointmentsRequest appointmentsRequest) {
        AppointmentEntity appointmentEntity = new AppointmentEntity();
        appointmentEntity.setSenderEmail(appointmentsRequest.getSenderEmail());
        appointmentEntity.setCandidateEmail(appointmentsRequest.getCandidateEmail());
        appointmentEntity.setTitle(appointmentsRequest.getTitle());
        appointmentEntity.setCc(StringUtils.stringConvert(appointmentsRequest.getCc(), ","));
        appointmentEntity.setBc(StringUtils.stringConvert(appointmentsRequest.getBc(), ","));
        appointmentEntity.setContent(appointmentsRequest.getContent());
        appointmentEntity.setInterviewers(appointmentsRequest.getInterviewers());
        appointmentEntity.setAgentSupport(appointmentsRequest.getAgentSupport());
        appointmentEntity.setCandidateID(appointmentsRequest.getCandidateID());
        appointmentEntity.setTopic(appointmentsRequest.getTopic());
        appointmentEntity.setDescription(appointmentsRequest.getDescription());
        appointmentEntity.setNode(1L);
        appointmentEntity.setFeedback(appointmentsRequest.getFeedback());
        appointmentEntity.setFinalDecision(appointmentsRequest.getFinalDecision());
        appointmentEntity.setObDate(DateUtils.toInstant(appointmentsRequest.getObDate()));
        appointmentEntity.setInterviewDate(DateUtils.toInstant(appointmentsRequest.getInterviewDate()));
        appointmentEntity.setRejectReason(appointmentsRequest.getRejectReason());
        appointmentEntity.setInterviewDuration(appointmentsRequest.getInterviewDuration());
        appointmentEntity.setApplicationTypeId(appointmentsRequest.getApplicantType());
        appointmentEntity.setIsDeleted(0);
        appointmentEntity.setIsReminded(0);
        appointmentEntity.setCreateDate(Instant.now());
        appointmentEntity = appointmentRepository.save(appointmentEntity);

        ApplicationTypeEntity applicationType = applicationTypeRepository.getById(appointmentEntity.getApplicationTypeId());
        CandidateEntity candidateEntity = candidateRepository.getById(appointmentEntity.getCandidateID());

        //send email to user address
        String content = "<!-- UUID --!>"
                + "<p>[CMC Global] Interview invitation _ " + applicationType.getType() + "&nbsp;" + candidateEntity.getPosition() + "</p>"
                + "<p>Dear," + "&nbsp;" + candidateEntity.getFirstName() + "&nbsp;" + candidateEntity.getName() + "</p>"
                + "<p>You have an upcoming interview. As discussed, we would like to " +
                "invite you to attend the interview with our company. Please have a look " +
                "and then attend the interview on time: </p>"
                + "<p>- Title:  " + applicationType.getType() + candidateEntity.getPosition() + "</p>"
                + "<br>"
                + "<p>- Date and time:  " + appointmentEntity.getInterviewDate() + "</p>"
                + "<br>"
                + "<p>- Interviewer:  " + appointmentEntity.getInterviewers() + "</p>"
                + "<br>"
                + "<p>- Contact point: " + appointmentEntity.getAgentSupport() + "</p>"
                + "<br>"
                + "<br>"
                + "<p>Notes: "
                + "<br>"
                + "<p> - Please confirm this invitation whether you would attend the interview. </p> "
                + "<p> - Should you have any further question, please do not hesitate to contact me via email anytime.</p>"
                + "<br>"
                + "<p>Yours Sincerely, </p>"
                + "<p>" + appointmentEntity.getAgentSupport() + "</p>";


        CustomMailSender.MailRequest mailRequest = new CustomMailSender.MailRequest();
        mailRequest.setTo(Collections.singletonList(appointmentEntity.getCandidateEmail()));
        mailRequest.setFrom("haivuhai184@gmail.com");
        mailRequest.setSubject("Remind email for upcoming interview from CMC");
        mailRequest.setContent(content);


        customMailSender.send(mailRequest, Throwable::printStackTrace);


        //-------------------

        return appointmentEntity;

    }


    @Override
    public AppointmentEntity deleteAppointmentEntity(Long id) {
        AppointmentEntity appointment = appointmentRepository.findById(id)
                .orElseThrow(
                        () -> new BusinessException(BaseResponseEnum.APPOINTMENT_NOT_FOUND));

        appointment.setIsDeleted(1);
        return appointmentRepository.save(appointment);
    }

    @SneakyThrows
    @Override
    public AppointmentEntity updateAppointmentEntity(Long id, UpdateAppointmentRequest updateAppointmentRequest) {
        AppointmentEntity appointmentEntity = appointmentRepository.findById(id).
                orElseThrow
                        (() -> new BusinessException(BaseResponseEnum.APPOINTMENT_NOT_FOUND));
        appointmentEntity.setSenderEmail(updateAppointmentRequest.getSenderEmail());
        appointmentEntity.setCandidateEmail(updateAppointmentRequest.getCandidateEmail());
        appointmentEntity.setTitle(updateAppointmentRequest.getTitle());
        appointmentEntity.setCc(StringUtils.stringConvert(updateAppointmentRequest.getCc(), ","));
        appointmentEntity.setBc(StringUtils.stringConvert(updateAppointmentRequest.getBc(), ","));
        appointmentEntity.setContent(updateAppointmentRequest.getContent());
        appointmentEntity.setInterviewers(updateAppointmentRequest.getInterviewers());
        appointmentEntity.setAgentSupport(updateAppointmentRequest.getAgentSupport());
        appointmentEntity.setCandidateID(updateAppointmentRequest.getCandidateID());
        appointmentEntity.setTopic(updateAppointmentRequest.getTopic());
        appointmentEntity.setDescription(updateAppointmentRequest.getDescription());
        appointmentEntity.setNode(updateAppointmentRequest.getNode());
        appointmentEntity.setFeedback(updateAppointmentRequest.getFeedback());
        appointmentEntity.setFinalDecision(updateAppointmentRequest.getFinalDecision());
        appointmentEntity.setObDate(DateUtils.toInstant(updateAppointmentRequest.getObDate()));
        appointmentEntity.setInterviewDate(DateUtils.toInstant(updateAppointmentRequest.getInterviewDate()));
        appointmentEntity.setRejectReason(updateAppointmentRequest.getRejectReason());
        appointmentEntity.setInterviewDuration(updateAppointmentRequest.getInterviewDuration());
        appointmentEntity.setApplicationTypeId(updateAppointmentRequest.getApplicantType());
        appointmentEntity.setUpdatedDate(Instant.now());
        return appointmentRepository.save(appointmentEntity);
    }


    @Override
    public PageDTO<AppointmentDTO> filterSearchAppointment(FilterCriteria criteria, String information, Pageable pageable) {
        Optional<CandyFilter> filterOpt = criteria.find(CandyFilter.CandyFilterEnum.UP_COMING);
        if (filterOpt.isPresent()) {
            Page<AppointmentDTO> appointmentDTOS = appointmentRepository.searchAppointment(
                    true,
                    CommonUtils.getInt(filterOpt.get().getFValue()),
                    information,
                    pageable);
            PageDTO<AppointmentDTO> result = filterOpt.map(candyFilter -> PageDTO.of(appointmentDTOS))
                    .orElseGet(
                            () ->
                                    PageDTO.of(appointmentDTOS)
                    );
            return result;
        }

        Optional<CandyFilter> filterOpt1 = criteria.find(CandyFilter.CandyFilterEnum.ALL_APPOINTMENT);
        Page<AppointmentDTO> appointmentDTOS = appointmentRepository.searchAppointment(
                false,
                CommonUtils.getInt(filterOpt1.get().getFValue()),
                information,
                pageable);
        PageDTO<AppointmentDTO> resultFalse = filterOpt1.map(candyFilter -> PageDTO.of(appointmentDTOS))
                .orElseGet(
                        () ->
                                PageDTO.of(appointmentDTOS)
                );

        return resultFalse;
    }

    @Override
    public AppointmentEntity giveFeedBack(Long id, UpdateAppointmentRequest updateAppointmentRequest) {
        AppointmentEntity appointmentEntity = appointmentRepository.findById(id).orElseThrow(
                () -> new BusinessException(BaseResponseEnum.APPOINTMENT_NOT_FOUND)
        );
        appointmentEntity.setNode(StatusEnum.INTERVIEW.getCode());
        appointmentEntity.setFeedback(updateAppointmentRequest.getFeedback());
        return appointmentRepository.save(appointmentEntity);
    }

    @Override
    public AppointmentEntity finalDecision(Long id, UpdateAppointmentRequest updateAppointmentRequest) {
        AppointmentEntity appointmentEntity = appointmentRepository.findById(id)
                .orElseThrow(
                        () -> new BusinessException(BaseResponseEnum.APPOINTMENT_NOT_FOUND)
                );
        appointmentEntity.setNode(StatusEnum.DECISION.getCode());
        appointmentEntity.setFinalDecision(updateAppointmentRequest.getFinalDecision());
        return appointmentRepository.save(appointmentEntity);
    }

    @Override
    public AppointmentEntity suspendInterview(Long id, UpdateAppointmentRequest updateAppointmentRequest) {
        AppointmentEntity appointmentEntity = appointmentRepository.findById(id)
                .orElseThrow(
                        () -> new BusinessException(BaseResponseEnum.APPOINTMENT_NOT_FOUND)
                );
        appointmentEntity.setInterviewDate(null);
        appointmentEntity.setInterviewDuration(0);
        return appointmentRepository.save(appointmentEntity);
    }

}
