package com.cmc.candy.candidate.entity.dto.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageDTO<T> {
    List<T> data;
    Long numberOfItems;
    long numberOfPages;

    public static <T> PageDTO<T> of(Page<T> page) {
        PageDTO<T> pageDTO = new PageDTO<>();
        pageDTO.setNumberOfItems(page.getTotalElements());
        pageDTO.setNumberOfPages(page.getTotalPages());
        pageDTO.setData(page.getContent());
        return pageDTO;
    }

    public static <T, R> PageDTO<R> of(Page<T> page, Function<T, R> mapper) {
        PageDTO<R> pageDTO = new PageDTO<>();
        pageDTO.setNumberOfItems(page.getTotalElements());
        pageDTO.setNumberOfPages(page.getTotalPages());
        pageDTO.setData(page.getContent().stream().map(mapper).collect(Collectors.toList()));
        return pageDTO;
    }
}
