package com.cmc.candy.candidate.entity.po;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.OffsetDateTime;


@Entity
@Table(name = "curriculum")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class CurriculumEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "candidate_id")
    Integer candidateID;

    @Column(name = "file_name")
    String fileName;

    @Column(name = "url")
    String url;

    @Column(name = "file_type")
    String fileType;

    @Column(name = "file_mime")
    String fileMime;

    @Column(name = "file_size")
    Long fileSize;

    @Column(name = "encrypt_algorithm")
    String encryptAlgorithm;

    @Column(name = "created_date")
    OffsetDateTime createdDate;
}
