package com.cmc.candy.candidate.entity.po;

import com.cmc.candy.appointment.entity.po.AppointmentEntity;
import com.cmc.candy.candidate.enums.PositionEnum;
import com.cmc.candy.candidate.enums.StatusEnum;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.Instant;


@Getter
@Setter
@Entity
@Table(name = "candidates")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class CandidateEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "first_name")
    String firstName;

    @Column(name = "last_name")
    String name;

    @Column(name = "candidate_email")
    String email;

    @Column(name = "phone_number")
    String phoneNumber;

    @Column(name = "skills")
    String skills;

    @Column(name = "date_of_birth")
    String dateOfBirth;

    @Column(name = "skype")
    String skype;

    @Column(name = "linkedin")
    String linkedin;

    @Column(name = "facebook")
    String facebook;

    @Column(name = "note")
    String note;

    @Column(name = "created_date")
    Instant createdDate;

    @Column(name = "updated_date")
    Instant updatedDate;

    @Column(name = "is_deleted")
    boolean isDeleted;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "candidates")
    private AppointmentEntity appointment;


    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    StatusEnum status;

    @Column(name = "position")
    @Enumerated(EnumType.STRING)
    PositionEnum position;


}
