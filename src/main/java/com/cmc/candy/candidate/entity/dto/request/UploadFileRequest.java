package com.cmc.candy.candidate.entity.dto.request;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UploadFileRequest {
    String fileName;
    String downloadUri;
    long fileSize;
    String fileType;
}