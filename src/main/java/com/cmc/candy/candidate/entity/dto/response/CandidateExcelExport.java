package com.cmc.candy.candidate.entity.dto.response;

import com.cmc.candy.candidate.entity.po.CandidateEntity;
import com.cmc.candy.candidate.entity.po.CandidateEntity_;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CandidateExcelExport {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<CandidateEntity> listCandidate;

    public CandidateExcelExport(List<CandidateEntity> listCandidate) {
        this.listCandidate = listCandidate;
        workbook = new XSSFWorkbook();
    }


    private void writeHeaderLine() {
        sheet = workbook.createSheet("Users");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);


        createCell(row, 0, "Candidate ID", style);
        createCell(row, 1, "first_name", style);
        createCell(row, 2, "last_name", style);
        createCell(row, 3, "phone_number", style);
        createCell(row, 4, "candidate_email", style);
        createCell(row, 5, "skills", style);
        createCell(row, 6, "date_of_birth", style);
        createCell(row, 7, "skype", style);
        createCell(row, 8, "linkedin", style);
        createCell(row, 9, "position", style);
        createCell(row, 10, "created_date", style);
        createCell(row, 11, "note", style);


    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (CandidateEntity candidate : listCandidate) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;
            createCell(row, columnCount++, candidate.getId().toString(), style);
            createCell(row, columnCount++, candidate.getFirstName(), style);
            createCell(row, columnCount++, candidate.getName(), style);
            createCell(row, columnCount++, candidate.getPhoneNumber().toString(), style);
            createCell(row, columnCount++, candidate.getEmail(), style);
            createCell(row, columnCount++, candidate.getSkills(), style);
            createCell(row, columnCount++, candidate.getDateOfBirth(), style);
            createCell(row, columnCount++, candidate.getSkype(), style);
            createCell(row, columnCount++, candidate.getLinkedin(), style);
            createCell(row, columnCount++, candidate.getPosition().name(), style);
            createCell(row, columnCount++, candidate.getCreatedDate().toString(), style);
            createCell(row, columnCount++, candidate.getNote(), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}
