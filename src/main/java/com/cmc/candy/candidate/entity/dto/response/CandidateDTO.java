package com.cmc.candy.candidate.entity.dto.response;

import com.cmc.candy.candidate.entity.po.CandidateEntity;
import com.cmc.candy.candidate.enums.PositionEnum;
import com.cmc.candy.candidate.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.Entity;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CandidateDTO {
    StatusEnum status;
    Long id;
    String firstName;
    String name;
    String email;
    String phoneNumber;
    String skills;
    PositionEnum candyPositionEntityId;

    public CandidateDTO(CandidateEntity candidateEntity) {
        this.status = candidateEntity.getStatus();
        this.id = candidateEntity.getId();
        this.firstName = candidateEntity.getFirstName();
        this.name = candidateEntity.getName();
        this.email = candidateEntity.getEmail();
        this.phoneNumber = candidateEntity.getPhoneNumber();
        this.skills = candidateEntity.getSkills();
        this.candyPositionEntityId = candidateEntity.getPosition();
    }

}
