package com.cmc.candy.candidate.entity.dto.response;

import lombok.AccessLevel;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.nio.file.Path;

@Getter
@Setter
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FileUploadResponse {
    String fileName;
    String downloadUri;
    long fileSize;
    String fileType;
    String url;
}
