package com.cmc.candy.candidate.entity.dto.request;

import com.cmc.candy.candidate.enums.PositionEnum;
import com.cmc.candy.candidate.enums.StatusEnum;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.OffsetDateTime;

@Data
public class CreateCandidateRequest {

    @Enumerated(EnumType.STRING)
    StatusEnum status;
    String firstName;
    String name;
    String email;
    String phoneNumber;
    String skills;
    String dateOfBirth;
    String skype;
    String linkedin;
    String facebook;
    String note;
    String isDeleted;
    PositionEnum position;
    Long curriculumId;
    OffsetDateTime startTime;
}
