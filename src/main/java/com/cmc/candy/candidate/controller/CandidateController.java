package com.cmc.candy.candidate.controller;

import com.cmc.candy.candidate.entity.dto.request.CreateCandidateRequest;
import com.cmc.candy.candidate.entity.dto.response.CandidateDTO;
import com.cmc.candy.candidate.entity.dto.response.CandidateExcelExport;
import com.cmc.candy.candidate.entity.dto.response.FileUploadResponse;
import com.cmc.candy.candidate.entity.dto.response.PageDTO;
import com.cmc.candy.candidate.entity.po.CandidateEntity;
import com.cmc.candy.candidate.repository.CandidateRepository;
import com.cmc.candy.candidate.service.CandidateService;
import com.cmc.candy.candidate.service.FileStoreService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
@CrossOrigin("*")

public class CandidateController {

    @Autowired
    CandidateService candidateService;

    @Autowired
    FileStoreService fileStoreService;

    @Value("${upload-dir}")
    String uploadDir;

    @GetMapping("/view-candidates/{id}")
    public ResponseEntity<CandidateEntity> viewCandidate(@PathVariable Long id) {
        log.info(" candidate id ==> {}", id);
        return ResponseEntity.ok(candidateService.viewCandidate(id));
    }

    @PostMapping("/candidates/create")
    public ResponseEntity<Void> createCandidate(@RequestBody CreateCandidateRequest request) {
        candidateService.createCandidate(request);
        return ResponseEntity.created(URI.create("url-reference")).build();
    }

    @PutMapping("/candidates/{id}")
    public ResponseEntity<Void> updateCandidate(@RequestBody CreateCandidateRequest request, @PathVariable("id") Long id) {
        log.info("updated candidate is ==> {} ,id ==> {}", request, id);
        candidateService.updateCandidate(id, request);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/candidates/delete/{id}")
    public ResponseEntity<Void> deleteCandidate(@PathVariable Long id) {
        log.info("id deleted is ==> {}", id);
        candidateService.deleteCandidate(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/candidates")
    public ResponseEntity<PageDTO<CandidateDTO>> listOutCandidateFilter(
            @RequestParam(value = "lastname", required = false) String query,
            @RequestParam(value = "email", required = false) String query2, Pageable pageable) {
        return ResponseEntity.ok(candidateService.filterCandidates(query, query2, pageable));
    }


    @PostMapping("/candidates/uploadCV")
    public ResponseEntity<FileUploadResponse> uploadFile(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(multipartFile.getOriginalFilename()));
        long fileSize = multipartFile.getSize();
        String fileType = multipartFile.getContentType();
        String fileCode = fileStoreService.saveFile(fileName, multipartFile);

        FileUploadResponse response = new FileUploadResponse();
        response.setFileName(fileName);
        response.setFileType(fileType);
        response.setFileSize(fileSize);

        response.setDownloadUri("/api/v1/candidates/downloadCV/?fileCode=" + fileCode);
        log.info("file part ==> {}", multipartFile);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/candidates/downloadCV")
    public ResponseEntity<?> downloadFile(@RequestParam("fileCode") String fileCode) {
        Resource resource = fileStoreService.getFileAsResource(fileCode)
                .orElseThrow(() -> new RuntimeException("Not found file !"));
        String headerValue = "attachment; filename=\"" + resource.getFilename() + "\"";
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, headerValue)
                .body(resource);
    }

    @GetMapping("/candidates/export-excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Candidate_List_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<CandidateEntity> listUsers = candidateService.listAllCandidate();

        CandidateExcelExport excelExport = new CandidateExcelExport(listUsers);

        excelExport.export(response);
    }


}
