package com.cmc.candy.candidate.enums;

public enum PositionEnum {
    SOFTWAREENGINEER, TESTER, TECHNICAL, LEAD, SYSTEMARCHITECTURE, BA, PM, HR
}
