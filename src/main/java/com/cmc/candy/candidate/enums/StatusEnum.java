package com.cmc.candy.candidate.enums;

public enum StatusEnum {
    BLACKLIST, AVAILABLE, PASSED, FAILED
}
