package com.cmc.candy.candidate.utils;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public final class QueryUtils {

    private QueryUtils() {
    }

    public static <T> Predicate filterLike(Root<T> root, CriteriaBuilder cb, String value, String... fields) {
        if (StringUtils.isEmpty(value)) {
            return cb.and();
        }
        List<Predicate> predicateList = new ArrayList<>();
        for (String field : fields) {
            predicateList.add(cb.like(root.get(field), "%".concat(value).concat("%")));
        }
        return cb.or(predicateList.toArray(new Predicate[0]));
    }

    public static <T, P> Predicate filterEqual(Root<T> root, CriteriaBuilder cb, P value, String... fields) {
        if (!CommonUtils.isEmpty(value)) {
            return cb.and();
        }
        List<Predicate> predicateList = new ArrayList<>();
        for (String field : fields) {
            predicateList.add(cb.equal(root.get(field), value));
        }
        return cb.or(predicateList.toArray(new Predicate[0]));
    }
}
