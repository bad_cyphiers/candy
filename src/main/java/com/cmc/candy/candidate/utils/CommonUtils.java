package com.cmc.candy.candidate.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;

public final class CommonUtils {

    private CommonUtils() {

    }

    public static boolean isEmpty(Object p) {
        if (p == null) return false;
        if (p instanceof String) {
            return StringUtils.isNotEmpty((String) p);
        }
        if (p instanceof Collection) {
            return !((Collection) p).isEmpty();
        }
        return true;
    }
}
