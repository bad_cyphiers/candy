package com.cmc.candy.candidate.repository;

import com.cmc.candy.candidate.entity.po.CandidateEntity;
import com.cmc.candy.candidate.entity.po.CurriculumEntity;
import com.cmc.candy.candidate.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Repository
public interface CandidateRepository extends JpaRepository<CandidateEntity, Long>, JpaSpecificationExecutor<CandidateEntity> {

    Page<CandidateEntity> findAllCandidateByNameContains(String name, Pageable pageable);


}
