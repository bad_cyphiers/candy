package com.cmc.candy.candidate.repository;

import com.cmc.candy.candidate.entity.po.CandidateEntity;
import com.cmc.candy.candidate.enums.StatusEnum;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CandidateRepositoryCustom extends CandidateRepository {
    @Query("SELECT c FROM CandidateEntity c WHERE upper(c.status) = upper(:status)"
            + "AND TIMESTAMPDIFF(MONTH, created_date, NOW()) >= :months")
    List<CandidateEntity> findAllCandidateWithStatusAndOverMonths(@Param("status") String status,
                                                                  @Param("months") int months);
}
