package com.cmc.candy.candidate.service;

import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.throwable.BusinessException;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

import static java.nio.file.Files.list;

@Service
public class FileStoreService {
    @Value("${upload-dir}")
    String uploadDir;

    public Optional<Resource> getFileAsResource(String fileCode) {
        try {
            Path dirPath = Paths.get(uploadDir);
            return list(dirPath).filter(file -> file.getFileName().toString().startsWith(fileCode)).findFirst().map(this::getUrlResource);
        } catch (IOException e) {
            throw new BusinessException(BaseResponseEnum.FILE_NOT_FOUND);
        }
    }

    @SneakyThrows
    public UrlResource getUrlResource(Path path) {
        return new UrlResource(path.toUri());
    }

    public String saveFile(String fileName, MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        String fileCode = RandomStringUtils.randomAlphanumeric(8);

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileCode + "-" + fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {
            throw new IOException("Could not save file: " + fileName, ioe);
        }
        return fileCode;

    }
}
