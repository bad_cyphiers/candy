package com.cmc.candy.candidate.service;

import com.cmc.candy.candidate.entity.dto.request.CreateCandidateRequest;
import com.cmc.candy.candidate.entity.dto.response.CandidateDTO;
import com.cmc.candy.candidate.entity.dto.response.PageDTO;
import com.cmc.candy.candidate.entity.po.CandidateEntity;
import com.cmc.candy.candidate.entity.po.CandidateEntity_;
import com.cmc.candy.candidate.repository.CandidateRepository;
import com.cmc.candy.candidate.utils.QueryUtils;
import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.throwable.BusinessException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CandidateService {

    @Resource
    ModelMapper modelMapper;

    @Resource
    CandidateRepository candidateRepository;

    public List<CandidateEntity> listAllCandidate() {
        return candidateRepository.findAll(Sort.by(CandidateEntity_.ID).ascending());
    }

    public CandidateEntity viewCandidate(Long id) {
        Optional<CandidateEntity> candidate = candidateRepository.findById(id);
        if (candidate.isPresent()) {
            return candidate.get();
        }
        throw new BusinessException(BaseResponseEnum.CANDIDATE_NOT_EXIST);
    }


    public void createCandidate(CreateCandidateRequest candidateRequest) {
        CandidateEntity candidateEntity = new CandidateEntity();
        BeanUtils.copyProperties(candidateRequest, candidateEntity);
        candidateEntity.setStatus(candidateRequest.getStatus());
        candidateEntity.setPosition(candidateRequest.getPosition());
        candidateEntity.setCreatedDate(Instant.now());
        candidateRepository.save(candidateEntity);
    }

    public void updateCandidate(Long id, CreateCandidateRequest candidateRequest) {
        CandidateEntity candidateEntity = candidateRepository.findById(id).orElseThrow(()
                -> new BusinessException(BaseResponseEnum.CANDIDATE_NOT_FOUND));

        candidateEntity.setEmail(candidateRequest.getEmail());
        candidateEntity.setFirstName(candidateRequest.getFirstName());
        candidateEntity.setName(candidateRequest.getName());
        candidateEntity.setPhoneNumber(candidateRequest.getPhoneNumber());
        candidateEntity.setSkills(candidateRequest.getSkills());
        candidateEntity.setDateOfBirth(candidateRequest.getDateOfBirth());
        candidateEntity.setSkype(candidateRequest.getSkype());
        candidateEntity.setLinkedin(candidateRequest.getLinkedin());
        candidateEntity.setFacebook(candidateRequest.getFacebook());
        candidateEntity.setNote(candidateRequest.getNote());
        candidateEntity.setPosition(candidateRequest.getPosition());
        candidateEntity.setUpdatedDate(Instant.now());
        candidateEntity.setStatus(candidateRequest.getStatus());

        candidateRepository.save(candidateEntity);
    }

    public void deleteCandidate(Long id) {
        Optional<CandidateEntity> optionalCandidate = candidateRepository.findById(id);
        if (!optionalCandidate.isPresent()) {
            throw new EntityNotFoundException("Candidate doesn't exist");
        }
        CandidateEntity candidateEntity = optionalCandidate.get();
        candidateEntity.setDeleted(true);
        candidateRepository.save(candidateEntity);

    }

    public PageDTO<CandidateDTO> filterCandidates(String name, String email, Pageable pageable) {
        Specification<CandidateEntity> specification = (root, cq, cb) ->
                cb.and(
                        QueryUtils.filterLike(root, cb, name, CandidateEntity_.NAME),
                        QueryUtils.filterEqual(root, cb, email, CandidateEntity_.EMAIL)
                );
        pageable.getSort().and(Sort.by(CandidateEntity_.NAME).ascending()).and(Sort.by(CandidateEntity_.EMAIL).descending());
        Page<CandidateEntity> candidates = candidateRepository.findAll(specification, pageable);
        return PageDTO.of(candidates, candidate -> modelMapper.map(candidate, CandidateDTO.class));
    }
}







