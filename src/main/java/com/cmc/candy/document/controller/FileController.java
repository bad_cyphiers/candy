package com.cmc.candy.document.controller;

import com.cmc.candy.candidate.entity.dto.response.FileUploadResponse;
import com.cmc.candy.candidate.service.FileStoreService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


@RestController
@RequestMapping("/api/v1/documents")
@CrossOrigin(origins = "*")
public class FileController {
    @Autowired
    FileStoreService fileStoreService;

    @GetMapping(value = "/previewPDF/{filecode}", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> previewPDF(@PathVariable("filecode") String fileCode) throws IOException {

        Resource resource = fileStoreService.getFileAsResource(fileCode)
                .orElseThrow(() -> new RuntimeException("Not found file !"));

        byte[] pdf = FileUtils.readFileToByteArray(resource.getFile());
        HttpHeaders headers = new HttpHeaders();
        String fileName = resource.getFilename();
        headers.setContentDispositionFormData("filename", fileName);
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setAcceptCharset(Collections.singletonList(StandardCharsets.UTF_8));
        return ResponseEntity.ok().headers(headers).body(pdf);
    }

    @PostMapping(value = "/file")
    public ResponseEntity<FileUploadResponse> uploadFile(@RequestParam("file") MultipartFile file,
                                                         HttpServletRequest httpServletRequest) throws IOException {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        long fileSize = file.getSize();
        String fileType = file.getContentType();
        String fileCode = fileStoreService.saveFile(fileName, file);

        FileUploadResponse response = new FileUploadResponse();
        response.setFileName(fileName);
        response.setFileType(fileType);
        response.setFileSize(fileSize);
        response.setUrl(httpServletRequest.getRequestURI() + "/" + fileCode);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/file/{filecode}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> viewFile(@PathVariable("filecode") String fileCode) throws IOException {

        Resource resource = fileStoreService.getFileAsResource(fileCode)
                .orElseThrow(() -> new RuntimeException("Not found file !"));

        byte[] image = FileUtils.readFileToByteArray(resource.getFile());
        HttpHeaders headers = new HttpHeaders();
        String fileName = resource.getFilename();
        headers.setContentDispositionFormData("filename", fileName);
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setAcceptCharset(Collections.singletonList(StandardCharsets.UTF_8));
        return ResponseEntity.ok().headers(headers).body(image);
    }


}

