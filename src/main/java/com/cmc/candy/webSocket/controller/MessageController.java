package com.cmc.candy.webSocket.controller;

import com.cmc.candy.webSocket.entity.request.SocketMessageRequest;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;


@Controller
@CrossOrigin("*")
public class MessageController {

    @MessageMapping("/message")
    @SendTo("/queue/reply")
    public String processMessageFromClient(@Payload SocketMessageRequest request) {
        return String.valueOf(request.getMessage());
    }

    @MessageExceptionHandler
    @SendTo("/queue/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }
}
