package com.cmc.candy.user.repository.custom;

import com.cmc.candy.user.entity.po.UserCandyEntity;
import com.cmc.candy.user.repository.UserRepository;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserRepositoryCustom extends UserRepository {

    @Transactional
    @Modifying
    @Query("UPDATE UserCandyEntity SET token = :token WHERE email = :email")
    void generateResetpasswordToken(@Param("token") String token, @Param("email") String email);

    /*
     *
     * */
    @Transactional
    @Modifying
    @Query("UPDATE UserCandyEntity SET password = :password, token = null, failedAttempt = 0 WHERE email = :email")
    void resetPassword(@Param("email") String email, @Param("password") String password);

    @Transactional
    @Modifying
    @Query("UPDATE UserCandyEntity SET isActive = :isActive WHERE userName = :userName")
    void updateIsActive(@Param("userName") String userName, @Param("isActive") boolean isActive);

    @Transactional
    @Modifying
    @Query("UPDATE UserCandyEntity SET failedAttempt = :failedAttempt WHERE userName = :userName")
    void updateFailedAttempt(@Param("failedAttempt") int failedAttempt, @Param("userName") String userName);

}
