package com.cmc.candy.user.repository;

import com.cmc.candy.user.entity.po.UserCandyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<UserCandyEntity, Long> {
    UserCandyEntity findByUserName(String username);

    Optional<UserCandyEntity> findByEmail(String email);

    Optional<UserCandyEntity> findByUserNameAndEmail(String userName, String email);

    List<UserCandyEntity> findByStatus(boolean email);

    @Query("SELECT u From UserCandyEntity u Where u.email = ?1")
    UserCandyEntity findByUserEmail(String email);

    @Query("SELECT u FROM UserCandyEntity u Where u.reset_password_token= ?1")
    UserCandyEntity findByResetPasswordToken(String token);


}
