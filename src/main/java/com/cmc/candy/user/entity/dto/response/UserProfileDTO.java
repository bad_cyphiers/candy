package com.cmc.candy.user.entity.dto.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class UserProfileDTO {
    Date dateOfBirth;
    String firstName;
    String lastName;
    String skype;
    String facebook;
    String phoneNumber;
    Date createdDate;
    Date updatedDate;
    String userName;
    String email;
    String password;
    String note;
    boolean status;
}
