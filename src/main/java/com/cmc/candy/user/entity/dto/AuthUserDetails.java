package com.cmc.candy.user.entity.dto;

import org.springframework.security.core.userdetails.UserDetails;

import java.time.Instant;

public interface AuthUserDetails extends UserDetails {

    String getRole();

    String getDob();

    String getFullName();
}
