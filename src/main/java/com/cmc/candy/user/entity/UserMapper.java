package com.cmc.candy.user.entity;

import com.cmc.candy.common.mapper.CommonMapper;
import com.cmc.candy.user.entity.dto.request.CreateUserRequest;
import com.cmc.candy.user.entity.po.UserCandyEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements CommonMapper<CreateUserRequest, UserCandyEntity> {

    @Override
    public UserCandyEntity toEntity(CreateUserRequest userRequest) {
        UserCandyEntity entity = UserCandyEntity.builder().build();
        BeanUtils.copyProperties(userRequest, entity);
        return entity;
    }

    @Override
    public CreateUserRequest toDTO(UserCandyEntity userCandyEntity) {
        CreateUserRequest dto = new CreateUserRequest();
        BeanUtils.copyProperties(userCandyEntity, dto);
        return dto;

    }
}
