package com.cmc.candy.user.entity.dto.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.math.BigInteger;
import java.util.Date;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserRequest {
    Long id;
    Date dateOfBirth;
    String skype;
    String facebook;
    BigInteger phoneNumber;
    Date createdDate;
    Date updatedDate;
    String userName;
    String email;
    String password;
    String note;
    boolean status;
    String token;
}
