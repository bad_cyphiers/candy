package com.cmc.candy.user.entity.dto.request;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class UserResetPasswordRequest {
    String username;
    String password;
    String email;
    String token;
}
