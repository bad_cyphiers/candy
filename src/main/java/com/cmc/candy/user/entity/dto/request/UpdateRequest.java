package com.cmc.candy.user.entity.dto.request;

import lombok.Data;

import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.Date;

@Data
public class UpdateRequest {
    String userName;
    String firstName;
    String lastName;
    OffsetDateTime dateOfBirth;
    String email;
    String password;
    String phoneNumeber;
    String skype;
    String faceBook;
    String note;
    Boolean status;
}
