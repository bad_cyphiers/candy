package com.cmc.candy.user.entity.dto.response;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigInteger;
import java.util.Date;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
public class UserResponseDTO {
    Long id;
    Date dateOfBirth;
    String firstName;
    String lastName;
    Date lastLogin;
    boolean isLocked;
    String skype;
    String facebook;
    String phoneNumber;
    Date createdDate;
    Date updatedDate;
    String userName;
    String email;
    String password;
    String note;
    boolean status;
    String token;
}
