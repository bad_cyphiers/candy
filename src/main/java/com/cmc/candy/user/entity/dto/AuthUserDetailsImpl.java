package com.cmc.candy.user.entity.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.time.Instant;
import java.util.Set;

@Data
@NoArgsConstructor
public class AuthUserDetailsImpl implements AuthUserDetails {

    String role;

    String dob;

    String fullName;

    Set<? extends GrantedAuthority> authorities;

    String password;

    String username;

    boolean isAccountNonExpired;

    boolean isAccountNonLocked;

    boolean isCredentialsNonExpired;

    boolean isEnabled;
}
