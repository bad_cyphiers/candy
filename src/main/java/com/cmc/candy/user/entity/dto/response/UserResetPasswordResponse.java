package com.cmc.candy.user.entity.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResetPasswordResponse {
    String username;
    String email;
    String token;
}
