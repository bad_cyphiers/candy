package com.cmc.candy.user.controller;


import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.throwable.BusinessException;
import com.cmc.candy.common.config.JwtTokenUtil;
import com.cmc.candy.user.entity.dto.request.JwtRequest;
import com.cmc.candy.user.entity.dto.request.RefreshTokenRequest;
import com.cmc.candy.user.entity.dto.response.JwtResponse;
import com.cmc.candy.user.entity.dto.response.UserResponseDTO;
import com.cmc.candy.user.entity.po.UserCandyEntity;
import com.cmc.candy.user.service.JwtUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin
@Slf4j
public class JwtAuthenticationController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    JwtUserDetailsService userDetailsService;

    @PostMapping("/authenticate")
    public ResponseEntity<JwtResponse> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) {
        try {
            userDetailsService.authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        } catch (CredentialsExpiredException ex) {
            // TODO bypass
            ex.printStackTrace();
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        String token = jwtTokenUtil.generateToken(userDetails);
        String refreshToken = jwtTokenUtil.generateRefreshToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token, refreshToken));
    }

    @PostMapping("/register")
    public ResponseEntity<UserCandyEntity> saveUser(@RequestBody UserResponseDTO user) {
        log.info("user regis ==> {}", user);
        return ResponseEntity.ok(userDetailsService.save(user));
    }

    @PostMapping("/refreshToken")
    public ResponseEntity<?> createValidToken(@RequestBody RefreshTokenRequest authenticationRequest) {
        Date expiration = jwtTokenUtil.getExpirationDateFromToken(authenticationRequest.getRefreshToken());
        if (!expiration.before(new Date())) {
            String userName = jwtTokenUtil.getUsernameFromToken(authenticationRequest.getRefreshToken());
            UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
            String token = jwtTokenUtil.generateToken(userDetails);
            return ResponseEntity.ok(new JwtResponse(token, authenticationRequest.getRefreshToken()));
        } else {
            throw new BusinessException(BaseResponseEnum.TOKEN_EXPIRED);
        }
    }
}
