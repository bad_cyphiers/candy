package com.cmc.candy.user.controller;

import com.cmc.candy.common.entity.ResponseDTO;
import com.cmc.candy.user.entity.dto.request.CreateUserRequest;
import com.cmc.candy.user.entity.dto.request.UserResetPasswordRequest;
import com.cmc.candy.user.entity.dto.response.UserResetPasswordResponse;
import com.cmc.candy.user.entity.dto.response.UserResponseDTO;
import com.cmc.candy.user.service.UserService;
import io.lettuce.core.dynamic.annotation.Param;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v2/resetPassword")
@Slf4j
@CrossOrigin("*")
@RequiredArgsConstructor
public class ResetPasswordController {
    private final UserService userService;

    @PostMapping
    public ResponseEntity<UserResetPasswordResponse> sentRequestResetPassword(@RequestBody UserResetPasswordRequest userRequest) {
        String username = userRequest.getUsername();
        String email = userRequest.getEmail();
        log.info("request reset username=>{}, email => {}", username, email);
        return ResponseEntity.ok(userService.sentRequestResetPassword(username, email));
    }

    @GetMapping
    public ResponseEntity<UserResetPasswordResponse> confirmRequestResetPassword(@RequestParam String username, @RequestParam String email, @RequestParam String token) {
        log.info("reset user password with username=>{}, email => {}, and token => {}", username, email, token);
        return ResponseEntity.ok(userService.confirmRequestResetPassword(username, email, token));
    }

    @PutMapping
    public ResponseEntity<UserResetPasswordResponse> resetPassword(@RequestBody UserResetPasswordRequest userRequest) {
        String username = userRequest.getUsername();
        String email = userRequest.getEmail();
        String password = userRequest.getPassword();
        String token = userRequest.getToken();
        log.info("update password with username=>{}, email => {}, password =>{}, token =>{}", username, email, password, token);
        return ResponseEntity.ok(userService.resetPassword(username, email, password, token));
    }
}
