package com.cmc.candy.user.controller;

import com.cmc.candy.common.entity.ResponseDTO;
import com.cmc.candy.user.entity.dto.request.CreateUserRequest;
import com.cmc.candy.user.entity.dto.request.ForgotRequest;
import com.cmc.candy.user.entity.dto.request.UpdatePasswordRequest;
import com.cmc.candy.user.entity.dto.request.UpdateRequest;
import com.cmc.candy.user.entity.dto.response.UserProfileDTO;
import com.cmc.candy.user.entity.dto.response.UserResetPasswordResponse;
import com.cmc.candy.user.entity.po.UserCandyEntity;
import com.cmc.candy.user.service.UserService;

import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/users")
@Slf4j
@CrossOrigin("*")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping()
    public ResponseDTO<Object> createUser(@RequestBody CreateUserRequest userRequest) {
        log.info("user => {}", userRequest);
        return ResponseDTO.builder().code("0").message("success").data(userService.createUser(userRequest)).build();
    }

    @GetMapping("{id}")
    public ResponseEntity<UserProfileDTO> viewProfile(@PathVariable Long id) {
        return ResponseEntity.ok(userService.viewProfile(id));
    }

    @PutMapping("{id}")
    public ResponseEntity<UserCandyEntity> updateProfile(@PathVariable Long id, @RequestBody UpdateRequest updateRequest) {
        return ResponseEntity.ok(userService.updateProfile(id, updateRequest));
    }

    @PostMapping("/forgot_password")
    public ResponseEntity showForgotPasswordFrom(@RequestBody ForgotRequest forgotRequest) {
        long TOKEN_VALIDITY = (long) 5 * 3 * 1000; // 5 minutes
        String token = RandomString.make(5) + ":" + System.currentTimeMillis() + TOKEN_VALIDITY;
        String email = forgotRequest.getEmail();
        userService.updateResetPasswordToken(token, email);
        userService.sendMail(email, token);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/reset_password")
    public ResponseEntity reSetPassword(@RequestBody UpdatePasswordRequest forgotRequest) {
        String token = forgotRequest.getToken();
        String email = forgotRequest.getEmail();
        String password = forgotRequest.getPassword();
        userService.updateResetPassword(token, email, password);
        return ResponseEntity.noContent().build();
    }

}
