package com.cmc.candy.user.service.impl;

import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.throwable.BusinessException;
import com.cmc.candy.common.utils.DateUtils;
import com.cmc.candy.common.utils.CustomMailSender;
import com.cmc.candy.common.utils.RedisUtils;
import com.cmc.candy.user.entity.UserMapper;
import com.cmc.candy.user.entity.dto.request.CreateUserRequest;
import com.cmc.candy.user.entity.dto.request.UpdateRequest;
import com.cmc.candy.user.entity.dto.response.UserProfileDTO;
import com.cmc.candy.user.entity.dto.response.UserResetPasswordResponse;
import com.cmc.candy.user.entity.dto.response.UserResponseDTO;
import com.cmc.candy.user.entity.po.UserCandyEntity;
import com.cmc.candy.user.repository.UserRepository;
import com.cmc.candy.user.repository.custom.UserRepositoryCustom;
import com.cmc.candy.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Configuration
public class UserServiceImpl implements UserService {

    private final RedisUtils redisUtils;
    private final PasswordEncoder bcryptEncoder;
    private final JavaMailSender javaMailSender;
    @Value("ngolong94ct@gmail.com")
    private String sender;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final CustomMailSender customMailSender;
    private final UserRepositoryCustom userRepositoryCustom;
    private final ModelMapper mapper;
    @Value("${spring.feature.user-reset-password.token.exp-in-sec}")
    private Long expInSec;
    @Value("${spring.feature.user-reset-password.server-uri}")
    private String serverUri;

    @Override
    public UserCandyEntity createUser(CreateUserRequest userRequest) {
        if (StringUtils.isNotBlank(userRequest.getUserName())) {
            throw new BusinessException(BaseResponseEnum.USER_CAN_NOT_NULL_OR_EMPTY);
        }

        UserCandyEntity user = userMapper.toEntity(userRequest);
        return userRepository.save(user);
    }

    @Override
    public UserProfileDTO viewProfile(Long id) {
        UserCandyEntity userCandyEntity = userRepository.findById(id)
                .orElseThrow(
                        () -> new BusinessException(BaseResponseEnum.USER_NOT_FOUND)
                );
        UserProfileDTO userProfileDTO = mapper.map(userCandyEntity,UserProfileDTO.class);
        return userProfileDTO;
    }

    @Override
    public UserCandyEntity updateProfile(Long id, UpdateRequest updateRequest) {
        UserCandyEntity userCandyEntity = userRepository.findById(id)
                .orElseThrow(
                        () -> new BusinessException(BaseResponseEnum.USER_NOT_FOUND)
                );
        userCandyEntity.setUserName(updateRequest.getUserName());
        userCandyEntity.setFirstName(updateRequest.getFirstName());
        userCandyEntity.setLastName(updateRequest.getLastName());
        userCandyEntity.setDateOfBirth(DateUtils.toInstant(updateRequest.getDateOfBirth()));
        userCandyEntity.setEmail(updateRequest.getEmail());
        userCandyEntity.setPassword(updateRequest.getPassword());
        userCandyEntity.setPhoneNumber(updateRequest.getPhoneNumeber());
        userCandyEntity.setUpdatedDate(Instant.now());
        userCandyEntity.setSkype(updateRequest.getSkype());
        userCandyEntity.setFacebook(updateRequest.getFaceBook());
        userCandyEntity.setNote(updateRequest.getNote());
        userCandyEntity.setStatus(updateRequest.getStatus());
        return userRepository.save(userCandyEntity);
    }

    @Override
    public UserResetPasswordResponse sentRequestResetPassword(String username, String email) {
        Optional<UserCandyEntity> userOpt = userRepository.findByUserNameAndEmail(username, email);
        if (!userOpt.isPresent()) {
            throw new BusinessException(BaseResponseEnum.USER_NOT_FOUND_1);
        }

        //generate a string token of 40 random numbers + datetime.now()
        String strOf40nums = "";
        for (int i = 0; i <= 3; i++) {
            strOf40nums += String.valueOf(Math.round(Math.random() * Math.pow(10, 11)));
        }
        String token = strOf40nums.concat(",").concat(LocalDateTime.now().toString());

        //update the new token to db (table "user")
        userRepositoryCustom.generateResetpasswordToken(token, email);

        //send email to user address
        String link = "" + serverUri + "?" + "username=" + username + "&email=" + email + "&token=" + token;
        System.out.println(link);
        String content = "<!-- UUID --!>"
                + "<p>Hello, <username></p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><a href=\"" + link + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";

        CustomMailSender.MailRequest mailRequest = new CustomMailSender.MailRequest();
        mailRequest.setTo(Collections.singletonList(email));
        mailRequest.setFrom("${spring.mail.username}");
        mailRequest.setSubject("Request to reset password from CMC");
        mailRequest.setContent(content);

        customMailSender.send(mailRequest, Throwable::printStackTrace);
        //-------------------

        // return
        return UserResetPasswordResponse.builder().username(username).email(email).build();

    }

    @Override
    public UserResetPasswordResponse confirmRequestResetPassword(String username, String email, String token) {
        Optional<UserCandyEntity> userOpt = userRepository.findByUserNameAndEmail(username, email);
        if (!userOpt.isPresent()) {
            throw new BusinessException(BaseResponseEnum.USER_NOT_FOUND_1);
        } else if (token != null && !userOpt.get().getToken().equals(token)) {
            throw new BusinessException(BaseResponseEnum.TOKEN_NOT_MATCH);
        } else {
            String[] s = token.split(",");
            LocalDateTime iat = LocalDateTime.parse(s[1]);
            Duration duration = Duration.between(iat, LocalDateTime.now()); //use getSeconds() to turn into seconds
            if (duration.getSeconds() > expInSec) {
                throw new BusinessException(BaseResponseEnum.TOKEN_EXPIRED);
            }
        }
        return UserResetPasswordResponse.builder().username(username).email(email).token(token).build();
    }

    @Override
    public UserResetPasswordResponse resetPassword(String username, String email, String password, String token) {
        Optional<UserCandyEntity> userOpt = userRepository.findByUserNameAndEmail(username, email);
        if (!userOpt.isPresent()) {
            throw new BusinessException(BaseResponseEnum.USER_NOT_FOUND_1);
        } else if (token != null && !userOpt.get().getToken().equals(token)) {
            throw new BusinessException(BaseResponseEnum.TOKEN_NOT_MATCH);
        }
        redisUtils.getAndDelete(username);
        userRepositoryCustom.resetPassword(email, bcryptEncoder.encode(password));
        return UserResetPasswordResponse.builder().email(email).build();
    }

    @Override
    public void updateResetPasswordToken(String token, String email) {

        UserCandyEntity userCandyEntity = userRepository.findByUserEmail(email);
        if (userCandyEntity != null) {
            userCandyEntity.setReset_password_token(token);
            userRepository.save(userCandyEntity);
        } else {
            System.out.println("Could not find any email belong to this customer");
        }
    }

    @Override
    public void updateResetPassword(String token, String email, String password) {
        try {
            UserCandyEntity userCandyEntity = userRepository.findByUserEmail(email);
            String[] t = userCandyEntity.getReset_password_token().split(":");
            String token_time = t[1];
            String token_check = t[0];
            long l = Long.parseLong(token_time);
            // userCandyEntity check
            if (token_check != null && token_check.equals(token)) {
                if (l >= System.currentTimeMillis()) {
                    redisUtils.getAndDelete(userCandyEntity.getUserName());
                    userCandyEntity.setReset_password_token(null);
                    userCandyEntity.setPassword(bcryptEncoder.encode(password));
                    userCandyEntity.setLoginRetryTime(null);
                    userCandyEntity.setActive(true);
                    userCandyEntity.setFailedAttempt(0);
                    userRepository.save(userCandyEntity);
                } else {
                    throw new BusinessException(BaseResponseEnum.TOKEN_EXPIRED);
                }
            } else {
                throw new BusinessException(BaseResponseEnum.COMMON_ERROR);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(BaseResponseEnum.NOT_PERMITTED);
        }
    }

    @Override
    public void sendMail(String email, String token) {
        try {
            // Creating a simple mail message
            SimpleMailMessage mailMessage
                    = new SimpleMailMessage();
            // Setting up necessary details
            String[] t = token.split(":");
            String token_check = t[0];
            mailMessage.setFrom(sender);
            mailMessage.setTo(email);
            mailMessage.setText(
                    "Hey!\n \n" +
                            "this is a simple email \n \n" +
                            "This is token refresh your password \n \n" +
                            token_check + "\n \n" +
                            " Thanks");
            mailMessage.setSubject("Simple email subject");
            // Sending the mail
            javaMailSender.send(mailMessage);
        }

        // Catch block to handle the exceptions
        catch (Exception e) {
            throw new BusinessException(BaseResponseEnum.FAIL_TO_SEND_EMAIL);
        }
    }
}
