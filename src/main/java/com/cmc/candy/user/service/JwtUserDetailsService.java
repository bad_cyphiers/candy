package com.cmc.candy.user.service;

import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.throwable.BusinessException;
import com.cmc.candy.common.utils.JsonUtils;
import com.cmc.candy.common.utils.RedisUtils;
import com.cmc.candy.user.entity.dto.AuthUserDetailsImpl;
import com.cmc.candy.user.entity.dto.response.UserResponseDTO;
import com.cmc.candy.user.entity.po.UserCandyEntity;
import com.cmc.candy.user.repository.UserRepository;
import com.cmc.candy.user.repository.custom.UserRepositoryCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;

@Service
@Slf4j
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    UserRepositoryCustom userRepositoryCustom;
    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder bcryptEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    RedisUtils redisUtils;

    public static final int MAX_FAILED_ATTEMPTS = 5;

    public UserCandyEntity save(UserResponseDTO userResponseDTO) {
        UserCandyEntity userCandyEntity = new UserCandyEntity();
        userCandyEntity.setUserName(userResponseDTO.getUserName());
        userCandyEntity.setFirstName(userResponseDTO.getFirstName());
        userCandyEntity.setLastName(userResponseDTO.getLastName());
        userCandyEntity.setLastLogin(Instant.now());
        userCandyEntity.setActive(true);
        userCandyEntity.setCreatedDate(Instant.now());
        userCandyEntity.setUpdatedDate(Instant.now());
        userCandyEntity.setEmail((userResponseDTO.getEmail()));
        userCandyEntity.setPassword(bcryptEncoder.encode(userResponseDTO.getPassword()));
        return userRepository.save(userCandyEntity);
    }

    public void authenticate(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new BusinessException(BaseResponseEnum.USER_DISABLE);
        } catch (BadCredentialsException e) {
            increaseFailedAttempt(username);
        } catch (LockedException L) {
            checkInTimeLock(username, password);
        }
        resetFailedAttempt(username);
    }

    public void increaseFailedAttempt(String username) {
        UserCandyEntity user = userRepository.findByUserName(username);
        if (user.getFailedAttempt() < MAX_FAILED_ATTEMPTS) {
            int newFailedAttempts = user.getFailedAttempt() + 1;
            userRepositoryCustom.updateFailedAttempt(newFailedAttempts, user.getUserName());
            throw new BusinessException((BaseResponseEnum.INVALID_CREDENTIALS));
        } else {
            user.setLoginRetryTime(Instant.now().plus(5, ChronoUnit.MINUTES));
            userRepository.save(user);
            userRepositoryCustom.updateIsActive(username, false);
            throw new BusinessException((BaseResponseEnum.OUT_TIMES_FAILED_LOGIN));
        }
    }

    public void resetFailedAttempt(String username) {
        UserCandyEntity user = userRepository.findByUserName(username);
        userRepositoryCustom.updateFailedAttempt(0, user.getUserName());
    }

    public void checkInTimeLock(String username, String password) {
        UserCandyEntity user = userRepository.findByUserName(username);
        if (user.getLoginRetryTime().compareTo(Instant.now()) < 0) {
            user.setLoginRetryTime(null);
            userRepository.save(user);
            userRepositoryCustom.updateIsActive(username, true);
            userRepositoryCustom.updateFailedAttempt(0, username);
            authenticate(username, password);
        } else {
            throw new BusinessException((BaseResponseEnum.IN_TIMES_LOCKED));
        }
    }

    @Override
    public UserDetails loadUserByUsername(String userName) {

        String userDetailsInRedis = redisUtils.get(userName);

        if (Objects.nonNull(userDetailsInRedis)) {
            return JsonUtils.parse(userDetailsInRedis, AuthUserDetailsImpl.class);
        }

        UserCandyEntity user = userRepository.findByUserName(userName);
        if (user == null) {
            throw new BusinessException(BaseResponseEnum.USER_NOT_FOUND);
        }

        log.info("Loading data {}", user);

        AuthUserDetailsImpl userDetails = new AuthUserDetailsImpl();
        userDetails.setUsername(user.getUserName());
        userDetails.setPassword(user.getPassword());
        userDetails.setAuthorities(Collections.emptySet());
        userDetails.setDob(Instant.now().toString());
        userDetails.setFullName(user.getFirstName() + " " + user.getLastName());
        userDetails.setAccountNonExpired(true);
        userDetails.setAccountNonLocked(user.isActive());
        userDetails.setEnabled(user.isActive());
        userDetails.setCredentialsNonExpired(user.isActive());
        redisUtils.put(userName, JsonUtils.toString(userDetails), 5);

        return userDetails;
    }

}
