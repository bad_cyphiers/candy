package com.cmc.candy.user.service;

import com.cmc.candy.user.entity.dto.request.CreateUserRequest;
import com.cmc.candy.user.entity.dto.request.UpdateRequest;
import com.cmc.candy.user.entity.dto.response.UserProfileDTO;
import com.cmc.candy.user.entity.dto.response.UserResetPasswordResponse;
import com.cmc.candy.user.entity.dto.response.UserResponseDTO;
import com.cmc.candy.user.entity.po.UserCandyEntity;

public interface UserService {
    UserCandyEntity createUser(CreateUserRequest userRequest);

    UserProfileDTO viewProfile(Long id);

    UserCandyEntity updateProfile(Long id, UpdateRequest updateRequest);

    UserResetPasswordResponse sentRequestResetPassword(String username, String email);

    UserResetPasswordResponse confirmRequestResetPassword(String username, String email, String token);

    UserResetPasswordResponse resetPassword(String username, String email, String password, String token);

    void updateResetPasswordToken(String token, String email);

    void sendMail(String email, String resetPasswordLink);

    void updateResetPassword(String token, String email, String password);

}
