package com.cmc.candy.common.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public enum BaseResponseEnum {

    COMMON_ERROR("999999", "error.generic"),

    USER_EXIST("000001", "user.create.user_exist"),
    USER_CAN_NOT_NULL_OR_EMPTY("000002", "user.create.userName_cannot_be_null_or_empty"),
    APPOINTMENT_NOT_FOUND("000003", "appointment.create.appointment_cannot_be_null_or_empty"),
    CANDIDATE_NOT_FOUND("000004", "candidate.update.candidate_not_found"),
    FILE_NOT_FOUND("000005", "candidate.file_not_found"),
    CANDIDATE_NOT_EXIST("000006", "candidate.view.candidate_not_exist"),
    USER_DISABLE("000007", "user.authenticate.disable"),
    INVALID_CREDENTIALS("000008", "user.authenticate.invalid_credentials"),
    USER_NOT_FOUND("000009", "user.authenticate.user_not_found"),
    CACHE_EXPIRED("0000010", "user.authenticate.cache_expired"),

    AUTHENTICATE_EXCEPTION("0000011", "auth.generic.auth_exception"),
    DATE_MUST_BE_BEFORE_TODAY("0000012", "generic.date_must_in_the_past"),
    DATE_MUST_BE_AFTER_TODAY("0000013", "generic.date_must_in_future"),
    DATE_NOT_NULL_OR_EMPTY("0000014", "generic.input_cannot_be_null_or_empty"),
    NOT_NULL("0000015", "generic_cannot_be_null"),

    USER_NOT_FOUND_1("0000016", "user.reset_password.user_not_found"),
    TOKEN_NOT_MATCH("0000017", "user.reset_password.token_not_match"),
    TOKEN_EXPIRED("0000018", "user.reset_password.token_expired"),
    NOT_PERMITTED("0000019", "user.had reset password"),

    CREDENTIAL_EXPIRED("0000012", "auth.generic.credential_expired"),

    OUT_TIMES_FAILED_LOGIN("0000015", "user.enter the wrong password more than the specified number of times "),
    IN_TIMES_LOCKED("00000016", "user.account is locked, Please contact admin to get more information"),

    FAIL_TO_SEND_EMAIL("00000017", "email sender and receiver has problem");


    String code;
    String value;
}
