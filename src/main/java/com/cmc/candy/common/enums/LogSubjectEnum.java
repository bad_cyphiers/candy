package com.cmc.candy.common.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public enum LogSubjectEnum {
    SEND_EMAIL_FAILED("[SEND_EMAIL_FAILED] => %s", 1L),
    ;
    String subjectName;
    long code;
}