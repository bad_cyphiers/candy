package com.cmc.candy.common.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public enum StatusEnum {

    RECRUITMENT("RECRUITMENT_NODE", 1L),
    INTERVIEW("INTERVIEW_NODE", 2L),
    DECISION("DECISION_NODE", 3L),
    ;

    String nodeName;
    long code;
}
