package com.cmc.candy.common.config;

import com.cmc.candy.common.entity.CandyErrorResponse;
import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.throwable.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Slf4j
public class ErrorHandler {

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusinessException(BusinessException ex, WebRequest request) {
        CandyErrorResponse errorResponse = new CandyErrorResponse(
                ex.getBaseResponseEnum().getCode(),
                ex.getBaseResponseEnum().getValue(),
                ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleGenericException(Exception ex, WebRequest request) {
        ex.printStackTrace();
        CandyErrorResponse errorResponse = new CandyErrorResponse(
                BaseResponseEnum.COMMON_ERROR.getCode(),
                BaseResponseEnum.COMMON_ERROR.getValue(),
                ex.getMessage()
        );
        return new ResponseEntity<>(errorResponse, HttpStatus.OK);
    }

}
