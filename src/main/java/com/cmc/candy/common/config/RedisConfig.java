package com.cmc.candy.common.config;

import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.throwable.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import redis.clients.jedis.Jedis;

@Configuration
@Slf4j
public class RedisConfig {

    @Value("${candy.redis.host}")
    String redisHost;

    @Value("${candy.redis.port}")
    int redisPort;

    @Value("${candy.redis.pass}")
    String redisPassword;

    @Bean
    JedisConnectionFactory jedisConnectionFactory() {
        log.info("Redis host = {}, redis port = {}, redis pass = {}", redisHost, redisPort, redisPassword);
        JedisConnectionFactory jedisConnectionFactory;
        try {
            RedisStandaloneConfiguration redisStandaloneConfiguration =
                    new RedisStandaloneConfiguration(redisHost, redisPort);

            if (!"none".equals(redisPassword)) {
                redisStandaloneConfiguration.setPassword(RedisPassword.of(redisPassword));
            }
            jedisConnectionFactory = new JedisConnectionFactory(redisStandaloneConfiguration);

            GenericObjectPoolConfig<Jedis> poolConfig = jedisConnectionFactory.getPoolConfig();
            if (poolConfig != null) {
                poolConfig.setMaxTotal(50);
                poolConfig.setMaxIdle(50);
            }

        } catch (RedisConnectionFailureException e) {
            e.printStackTrace();
            throw new BusinessException(BaseResponseEnum.CACHE_EXPIRED);
        }

        return jedisConnectionFactory;
    }

    @Bean
    RedisTemplate<String, Object> redisTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        // There are 2 types of connection factory
        // 1st is lettuce, 2nd is jedis
        // in this project, we use jedis
        template.setConnectionFactory(jedisConnectionFactory());
        template.setValueSerializer(new GenericToStringSerializer<>(Object.class));
        return template;
    }
}
