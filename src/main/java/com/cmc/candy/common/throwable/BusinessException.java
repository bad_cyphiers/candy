package com.cmc.candy.common.throwable;

import com.cmc.candy.common.enums.BaseResponseEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BusinessException extends RuntimeException {

    private final BaseResponseEnum baseResponseEnum;

}
