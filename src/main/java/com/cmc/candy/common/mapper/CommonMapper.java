package com.cmc.candy.common.mapper;

public interface CommonMapper<D, E> {

    E toEntity(D d);

    D toDTO(E e);
}
