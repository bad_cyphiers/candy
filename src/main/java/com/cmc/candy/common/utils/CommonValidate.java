package com.cmc.candy.common.utils;

import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.throwable.BusinessException;
import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.time.OffsetDateTime;

@UtilityClass
public class CommonValidate {
    public void dateBefore(OffsetDateTime offsetDateTime) {
        if (null == offsetDateTime) {
            throw new BusinessException(BaseResponseEnum.DATE_NOT_NULL_OR_EMPTY);
        }
        if (offsetDateTime.isAfter(DateUtils.toOffsetDateTime(Instant.now()))) {
            throw new BusinessException(BaseResponseEnum.DATE_MUST_BE_BEFORE_TODAY);
        }
    }

    public void dateAfter(OffsetDateTime offsetDateTime) {
        if (null == offsetDateTime) {
            throw new BusinessException(BaseResponseEnum.DATE_NOT_NULL_OR_EMPTY);
        }
        if (offsetDateTime.isBefore(DateUtils.toOffsetDateTime(Instant.now()))) {
            throw new BusinessException(BaseResponseEnum.DATE_MUST_BE_AFTER_TODAY);
        }
    }

    public void inputNotNull(String value) {
        if (value.isEmpty()) {
            throw new BusinessException(BaseResponseEnum.NOT_NULL);
        }
    }

}
