package com.cmc.candy.common.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class JsonUtils {

    private static final Gson PARSER = new Gson();

    public static String toString(Object object) {
        if (object == null) {
            return "";
        }
        return PARSER.toJson(object);
    }

    public static <T> T parse(String objStr, Class<T> clazz) {
        if (objStr == null) {
            return null;
        }

        return PARSER.fromJson(objStr, clazz);
    }

    public static Object getValue(JsonObject object, String key) {

        if (object == null) {
            return null;
        }

        if (key == null) {
            return null;
        }

        return object.get(key) != null ? object.get(key).getAsString() : null;
    }
}
