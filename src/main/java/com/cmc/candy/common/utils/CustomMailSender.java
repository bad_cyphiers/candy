package com.cmc.candy.common.utils;

import com.cmc.candy.common.enums.LogSubjectEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Component
@Slf4j

public class CustomMailSender {
    @Autowired
    JavaMailSender mailSender;

    @Async
    public void send(MailRequest mailRequest, ErrorCallback errorCallback) {
        try {
            final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
            message.setFrom(mailRequest.getFrom());
            message.setSubject(mailRequest.getSubject());
            message.setText(mailRequest.getContent(), true);

            for (String to : mailRequest.getTo()) {
                try {
                    InternetAddress address = new InternetAddress(to);
                    address.validate();
                    message.addTo(address);
                } catch (AddressException ex) {
                    log.warn(String.format(LogSubjectEnum.SEND_EMAIL_FAILED.getSubjectName(), ex.getMessage()));
                }
            }

            if (!CollectionUtils.isEmpty(mailRequest.getCc())) {
                for (String cc : mailRequest.getCc()) {
                    message.addCc(cc);
                }
            }

            if (!CollectionUtils.isEmpty(mailRequest.getBcc())) {
                for (String bcc : mailRequest.getBcc()) {
                    message.addBcc(bcc);
                }
            }
            this.mailSender.send(mimeMessage);
        } catch (MailSendException ex1) {
            if (ex1.getMessage().contains("Invalid Addresses")) {
                log.warn(String.format(LogSubjectEnum.SEND_EMAIL_FAILED.getSubjectName(),ex1.getMessage()));
            } else if (ex1.getMessage().contains("No recipient addresses")) {
                log.warn(String.format(LogSubjectEnum.SEND_EMAIL_FAILED.getSubjectName(),ex1.getMessage()));
            }
        } catch (Exception e) {
            errorCallback.onError(e);
        }
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MailRequest {
        List<String> to;
        List<String> cc;
        List<String> bcc;
        String from;
        String subject;
        String content;
    }

    @FunctionalInterface
    public interface ErrorCallback {
        void onError(Exception exception);
    }
}
