package com.cmc.candy.common.utils;

import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.throwable.BusinessException;
import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;

@UtilityClass
public class DateUtils {

    public Instant toInstant(OffsetDateTime offsetDateTime) {
        if (null == offsetDateTime) {
            return null;
        }
        return offsetDateTime.toInstant();
    }

    public OffsetDateTime toOffsetDateTime(Instant instant) {
        if (null == instant) {
            return null;
        }
        return OffsetDateTime.ofInstant(instant, ZoneId.systemDefault());
    }
}
