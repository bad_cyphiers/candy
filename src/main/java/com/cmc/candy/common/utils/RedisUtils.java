package com.cmc.candy.common.utils;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Component
@FieldDefaults(level = AccessLevel.MODULE, makeFinal = true)
public class RedisUtils {

    RedisTemplate<String, String> redisTemplate;
    ValueOperations<String, String> redisOperations;

    RedisUtils(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
        redisOperations = redisTemplate.opsForValue();
    }

    public void put(String key, String value, long expireTime) {
        redisOperations.set(key, value, Duration.of(expireTime, ChronoUnit.MINUTES));
    }

    public String get(String key) {
        return redisOperations.get(key);
    }

    public String getAndDelete(String key) {
        return redisOperations.getAndDelete(key);
    }

}
