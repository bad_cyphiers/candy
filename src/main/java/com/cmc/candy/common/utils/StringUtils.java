package com.cmc.candy.common.utils;

import lombok.experimental.UtilityClass;
import org.springframework.util.CollectionUtils;

import java.util.List;

@UtilityClass
public class StringUtils {

    public String stringConvert(List<String> items, String separator) {
        if (CollectionUtils.isEmpty(items)) {
            return "";
        }

        if (items.size() == 1) {
            return items.get(0);
        }

        if (org.apache.commons.lang3.StringUtils.isBlank(separator)) {
            return String.join(",", items);
        }

        if (org.apache.commons.lang3.StringUtils.isBlank(separator)) {
            return String.join(",", items);
        }

        return String.join(separator, items);
    }
}
