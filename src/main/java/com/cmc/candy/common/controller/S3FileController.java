package com.cmc.candy.common.controller;

import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.cmc.candy.common.enums.BaseResponseEnum;
import com.cmc.candy.common.service.FileService;
import com.cmc.candy.common.throwable.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/files")
@CrossOrigin("*")
public class S3FileController {
    private static final String MESSAGE_1 = "Uploaded the file successfully";

    @Autowired
    FileService fileService;

    @GetMapping("/download")
    public ResponseEntity<Object> downloadFromS3Bucket(@RequestParam("fileName") String fileName) {
        InputStreamResource input;
        try {
            S3ObjectInputStream ins = fileService.findByName(fileName);
            input = new InputStreamResource(ins);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BusinessException(BaseResponseEnum.FILE_NOT_FOUND);
        }

        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.noCache())
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + fileName + "\"")
                .body(input);
    }

    @PostMapping
    public ResponseEntity<Object> save(@RequestParam("file") MultipartFile multipartFile) {
        fileService.save(multipartFile);
        return new ResponseEntity<>(MESSAGE_1, HttpStatus.OK);
    }

}
