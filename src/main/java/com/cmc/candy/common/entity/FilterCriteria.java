package com.cmc.candy.common.entity;

import java.util.List;
import java.util.Optional;

public interface FilterCriteria {

    FilterCriteria build(CandyFilter candyFilter);

    List<CandyFilter> toFilterList();

    Optional<CandyFilter> find(CandyFilter.CandyFilterEnum key);
}
