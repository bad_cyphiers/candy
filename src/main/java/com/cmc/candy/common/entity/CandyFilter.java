package com.cmc.candy.common.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Builder(toBuilder = true)
@Getter
@Jacksonized
public class CandyFilter {

    /**
     * Support array filter
     */
    CandyFilterEnum fKey;

    /**
     * Must be matched criteria condition
     */
    String fName;

    /**
     * Filter value
     */
    String fValue;

    public enum CandyFilterEnum {
        UP_COMING, ALL_APPOINTMENT, NAME, TITLE
    }
}
