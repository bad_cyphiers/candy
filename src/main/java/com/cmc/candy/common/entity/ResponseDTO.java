package com.cmc.candy.common.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseDTO<T> {
    private String code;
    private String message;
    private T data;
}
