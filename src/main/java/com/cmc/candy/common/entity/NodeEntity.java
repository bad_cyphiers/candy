package com.cmc.candy.common.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "status_nodes")
@Setter
@Getter
public class NodeEntity {
    @Id
    private Long id;
    @Column(name = "node_name")
    private String nodeName;
}
