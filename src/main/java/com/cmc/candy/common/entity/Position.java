package com.cmc.candy.common.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "position")
@Getter
@Setter
public class Position {
    @Id
    private Long id;
    @Column(name = "position_name")
    private String positionName;
}
