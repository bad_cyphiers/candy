package com.cmc.candy.common.entity;

import java.time.OffsetDateTime;


public class CandyEntity {

    Long id;
    OffsetDateTime startDate;
    OffsetDateTime modifiedDate;

    public Long getId() {
        return id;
    }

    public OffsetDateTime getModifiedDate() {
        return modifiedDate;
    }

    public OffsetDateTime getStartDate() {
        return startDate;
    }
}
