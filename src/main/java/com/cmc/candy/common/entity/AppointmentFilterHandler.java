package com.cmc.candy.common.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AppointmentFilterHandler implements FilterCriteria {

    private final List<CandyFilter> candyFilters;

    public AppointmentFilterHandler() {
        candyFilters = new ArrayList<>();
    }

    public AppointmentFilterHandler(List<CandyFilter> candyFilters) {
        this.candyFilters = candyFilters;
    }

    @Override
    public FilterCriteria build(CandyFilter candyFilter) {
        candyFilters.add(candyFilter);
        return this;
    }

    @Override
    public List<CandyFilter> toFilterList() {
        return candyFilters;
    }

    @Override
    public Optional<CandyFilter> find(CandyFilter.CandyFilterEnum key) {
        return candyFilters.stream()
                .filter(e -> e.getFKey().equals(key))
                .findFirst();
    }


}
