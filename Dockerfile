#For Java 8 alpine
FROM openjdk:8-jdk-alpine

# Environent variables
ENV ACTIVE_PROFILE=staging

# Refer to Maven build -> finalName
ARG JAR_FILE=target/candy-0.0.1-SNAPSHOT.jar

# Author
MAINTAINER tmnhat <trinhminhnhat2011@gmail.com>

# cp target/mrbs-0.0.1-SNAPSHOT.jar mrbs.jar
COPY ${JAR_FILE} candy.jar

# java -jar /opt/app/mrbs.jar
ENTRYPOINT ["java","-jar", "-Dspring.profiles.active=${ACTIVE_PROFILE}", "candy.jar"]

#Exposed port
EXPOSE 8080
